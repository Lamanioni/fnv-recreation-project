extends Node

var human_companion: bool = false
var robot_companion: bool = false


func can_human_companion_join() -> bool:
	if human_companion:
		return false
	human_companion = true
	return true


func can_robot_companion_join() -> bool:
	if robot_companion:
		return false
	robot_companion = true
	return true
