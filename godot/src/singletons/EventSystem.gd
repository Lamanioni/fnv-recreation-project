extends Node

onready var dialogue_box: Label = get_tree().get_nodes_in_group("DialogueBox")[0]
onready var dialogue_options: VBoxContainer = get_tree().get_nodes_in_group("DialogueOptions")[0]

signal _done

var next_dialogue_timer: Timer
var option_index: int = -1


func _ready() -> void:
	next_dialogue_timer = Timer.new()
	add_child(next_dialogue_timer)

	next_dialogue_timer.autostart = false
	next_dialogue_timer.wait_time = 3.0

	var _connect: int = next_dialogue_timer.connect("timeout", self, "_finish_interaction")


func _input(event: InputEvent) -> void:
	if dialogue_options.visible:
		return

	if event.is_action_pressed("ui_accept"):
		_finish_interaction()


func say_dialogue(dialogue: String) -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	dialogue_box.visible = true
	dialogue_box.text = dialogue
	next_dialogue_timer.start()


func show_dialogue_options(options: Array) -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	dialogue_options.visible = true

	for option in range(options.size()):
		var button: Button = Button.new()
		dialogue_options.add_child(button)

		button.set_text(options[option])
		button.set_name(str(option))
		button.set_size(Vector2(800, 50))

		var _connect: int = button.connect(
			"pressed", self, "_finish_interaction", [int(button.name)]
		)


func _finish_interaction(index: int = -1) -> void:
	dialogue_box.visible = false
	dialogue_options.visible = false
	next_dialogue_timer.stop()

	for child in dialogue_options.get_children():
		child.queue_free()

	option_index = index

	emit_signal("_done")
