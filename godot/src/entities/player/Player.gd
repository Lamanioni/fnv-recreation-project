extends KinematicBody

const _SPEED: int = 15
const _ACCELERATION: int = 10

const _AIR_ACCELERATION: int = 3
const _GRAVITY: int = 30
const _JUMP_FORCE: int = 12

const _MOUSE_SENSITIVITY: float = 0.1
const _CAMERA_ACCELERATION: int = 40

onready var _acceleration: int = _ACCELERATION

var _velocity: Vector3 = Vector3.ZERO
var _gravity: Vector3 = Vector3.ZERO

onready var _head: Spatial = $Head
onready var _camera: Camera = $Head/Eye
onready var _interacter: RayCast = $Head/InteractRay
onready var _bonker: int = $HeadHitbox.connect("body_entered", self, "_handle_bonk")
onready var _hit_slow: int = $OtherHitbox.connect("body_entered", self, "_handle_slowdown")
onready var _unhit_slow: int = $OtherHitbox.connect("body_exited", self, "_handle_slowdown")


func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event: InputEvent) -> void:
	if EventSystem.dialogue_box.visible or EventSystem.dialogue_options.visible:
		return

	if event.is_action_pressed("interact"):
		_handle_interaction()

	if event.is_action_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	if event is InputEventMouseButton:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	if event is InputEventMouseMotion:
		rotate_y(deg2rad(-event.relative.x * _MOUSE_SENSITIVITY))
		_head.rotate_x(deg2rad(-event.relative.y * _MOUSE_SENSITIVITY))
		_head.rotation.x = clamp(_head.rotation.x, deg2rad(-89), deg2rad(89))


func _process(delta: float) -> void:
	if EventSystem.dialogue_box.visible or EventSystem.dialogue_options.visible:
		var target: Vector3 = _interacter.get_collider().get_parent().global_transform.origin
		target.y += 1.5
		_camera.look_at(target, Vector3.UP)
		return

	if Engine.get_frames_per_second() > Engine.iterations_per_second:
		_camera.set_as_toplevel(true)
		_camera.global_transform.origin = _camera.global_transform.origin.linear_interpolate(
			_head.global_transform.origin, _CAMERA_ACCELERATION * delta
		)
		_camera.rotation.y = rotation.y
		_camera.rotation.x = _head.rotation.x
	else:
		_camera.set_as_toplevel(false)
		_camera.global_transform = _head.global_transform


func _physics_process(delta: float) -> void:
	if EventSystem.dialogue_box.visible or EventSystem.dialogue_options.visible:
		return

	_move_and_jump(delta)


# Main movement function for the player
func _move_and_jump(delta: float) -> void:
	var horizontal_rotation: float = global_transform.basis.get_euler().y

	var vertical_axis: float = (
		Input.get_action_strength("move_backward")
		- Input.get_action_strength("move_forward")
	)

	var horizontal_axis: float = (
		Input.get_action_strength("move_right")
		- Input.get_action_strength("move_left")
	)

	var direction: Vector3 = Vector3(horizontal_axis, 0, vertical_axis).rotated(Vector3.UP, horizontal_rotation).normalized()
	var snap: Vector3 = Vector3.ZERO

	if is_on_floor():
		snap = -get_floor_normal()
		_acceleration = _ACCELERATION
		_gravity = Vector3.ZERO
	else:
		snap = Vector3.DOWN
		_acceleration = _AIR_ACCELERATION
		_gravity += Vector3.DOWN * _GRAVITY * delta

	if Input.is_action_just_pressed("jump") and is_on_floor():
		snap = Vector3.ZERO
		_gravity = Vector3.UP * _JUMP_FORCE

	_velocity = _velocity.linear_interpolate(direction * _SPEED, _acceleration * delta)

	var movement: Vector3 = _velocity + _gravity
	var _move: Vector3 = move_and_slide_with_snap(movement, snap, Vector3.UP)


# When the player is looking at something interactable, this is being handled
func _handle_interaction() -> void:
	if not _interacter.is_colliding():
		return

	var character: Node = _interacter.get_collider().get_parent()
	print("Interacting with " + character.name)

	character.handle_interaction()


# When the player bonks their _head into a ceiling, make them fall
func _handle_bonk(_body: Node):
	_gravity *= 0.1


# When the player hits something, cause them to slow down
func _handle_slowdown(_body: Node):
	_velocity *= 0.1
