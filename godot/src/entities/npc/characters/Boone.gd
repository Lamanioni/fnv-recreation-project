extends KinematicBody

const SPEED: int = 10

onready var navigation: Navigation = get_parent()
onready var player: KinematicBody = get_tree().get_nodes_in_group("Player")[0]

onready var _timer_connect: int = $Timer.connect("timeout", self, "_get_path_to_target")

var path: PoolVector3Array


func _ready() -> void:
	set_physics_process(false)


func _physics_process(delta: float) -> void:
	if global_transform.origin.distance_to(player.global_transform.origin) < 7:
		return

	var distance: float = SPEED * delta
	var last_point: Vector3 = global_transform.origin

	for _i in range(path.size()):
		var distance_to_next: float = last_point.distance_to(path[0])
		if distance <= distance_to_next and distance >= 0:
			global_transform.origin = last_point.linear_interpolate(
				path[0], distance / distance_to_next
			)
			# var _collide: KinematicCollision = move_and_collide(Vector3.ZERO)
			break
		elif path.size() == 1 and distance >= distance_to_next:
			global_transform.origin = path[0]
			set_physics_process(false)
			break

		distance -= distance_to_next
		last_point = path[0]
		path.remove(0)


func _get_target():
	return player


func _get_path_to_target():
	if global_transform.origin.distance_to(player.global_transform.origin) < 5:
		return

	var target: Vector3 = _get_target().global_transform.origin
	path = navigation.get_simple_path(global_transform.origin, target)

	set_physics_process(true)


func _speak(dialogue: String):
	EventSystem.say_dialogue(dialogue)
	yield(EventSystem, "_done")


func handle_interaction() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


	EventSystem.say_dialogue("Thank you for helping out.")
	yield(EventSystem, "_done")

	EventSystem.say_dialogue("What can I do for your kindness?")
	yield(EventSystem, "_done")

	EventSystem.show_dialogue_options(
		[
			"No, problem.",
			"I thought there is some reward to it.",
			"I did it for you, Boone. Come with me for a purpose!"
		]
	)
	yield(EventSystem, "_done")

	if EventSystem.option_index == 0:
		EventSystem.say_dialogue("Yeah, right.")
		yield(EventSystem, "_done")
	elif EventSystem.option_index == 1:
		EventSystem.say_dialogue("I'm sorry, but I can't offer you anything right now...")
		yield(EventSystem, "_done")
	elif EventSystem.option_index == 2:
		EventSystem.say_dialogue("No, I think it's time for me to rest. But still, thank you.")
		yield(EventSystem, "_done")

		EventSystem.show_dialogue_options(
			[
				"I need you, Boone. I cannot go on my quest alone.",
				"I'm sorry for your loss. Goodbye then."
			]
		)
		yield(EventSystem, "_done")

		if EventSystem.option_index == 0:
			EventSystem.say_dialogue("No, I am over with th- you know what, fine.")
			yield(EventSystem, "_done")

			if PlayerStats.can_human_companion_join():
				EventSystem.say_dialogue(
					"But if you turn out to be someone unpleasent, I wouldn't follow you any longer than that."
				)
				yield(EventSystem, "_done")
				set_physics_process(true)
				PlayerStats.human_companion = true
				$Timer.start()
			else:
				EventSystem.say_dialogue(
					"Well, I would if I could, it seems like the team would be just one too many."
				)
				yield(EventSystem, "_done")

		elif EventSystem.option_index == 1:
			EventSystem.say_dialogue("Yeah, thanks again for helping.")
			yield(EventSystem, "_done")
